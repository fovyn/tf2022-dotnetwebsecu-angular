import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';

const APP_ROUTE: Routes = [
    {
        path: 'product',
        loadChildren: () => import("./modules/produit/produit.module").then(m => m.ProduitModule)
    },
    {
        path: 'shop',
        loadChildren: () => import('./modules/shop/shop.module').then(m => m.ShopModule)
    }
]

@NgModule({
    imports: [RouterModule.forRoot(APP_ROUTE)],
    exports: [RouterModule]
})

export class AppRoutingModule { }