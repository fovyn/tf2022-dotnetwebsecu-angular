import { Component, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { UserFormComponent } from './components/user-form/user-form.component';
import { UserListComponent } from './components/user-list/user-list.component';
import { MatModalDirective } from './modules/shared/directives/mat-modal.directive';
import { LoginModalComponent } from 'src/app/modules/shared/modals/login-modal/login-modal.component';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    @ViewChild(UserListComponent)
    private list: UserListComponent | null = null;

    @ViewChild(UserFormComponent)
    private form: UserFormComponent | null = null;

    @ViewChild(MatModalDirective)
    private modal: MatModalDirective | null = null;
    get LoginModalComponent(): any { return LoginModalComponent }

    private index: number = -1;

    title = 'tf2022dotnetsecu';

    ngOnInit() {
        // this.modal?.open();
    }


    openModal() {
        this.modal?.open();
    }
    handleNewUser(newUser: any) {
        this.list?.addUser(newUser);

        this.modal?.open();
    }

    handleRemove() {
        // this.list?.removeUser({ username: 'Flavian' })
    }

    handleEditUser(u: { user: any, index: number }) {
        if (this.form) {
            this.form.Form = u.user;
            this.index = u.index;
            this.form.isEdit = true;
        }
    }

    handleUpdateUser(u: any) {
        this.list?.updateUser(this.index, u);
        console.log(u);
    }
}
