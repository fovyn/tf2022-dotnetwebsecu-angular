import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormInputComponent } from '../shared/components/form-input/form-input.component';
import { FormsModule } from '@angular/forms';



@NgModule({
    declarations: [

    ],
    imports: [
        CommonModule,
    ],
    exports: [
    ]
})
export class CoreModule { }
