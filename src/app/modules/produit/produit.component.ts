import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from 'src/app/models/product.model';

@Component({
    selector: 'produit',
    templateUrl: './produit.component.html',
    styleUrls: ['./produit.component.scss']
})
export class ProduitComponent implements OnInit {
    private bucket: { [key: string]: number } = {};

    get Bucket(): Array<{ key: string, qtt: number }> {
        const array: Array<{ key: string, qtt: number }> = [];
        for (let key in this.bucket) {
            array.push({ key, qtt: this.bucket[key] });
        }
        return array;
    }

    get TotalBucket(): number {
        let total = 0;
        const a = this.Bucket;

        for (let item of a) {
            total += item.qtt
        }

        return total;
    }

    get BucketItem(): number {
        let nb = 0;

        for (let i in this.bucket) {
            nb++;
        }

        return nb;
    }

    constructor(
        private $router: Router
    ) { }

    ngOnInit(): void {
    }


    handleBuyEvent(data: { product: Product, amount: number }) {
        const { product, amount } = data;
        if (!this.bucket[product.id]) {
            this.bucket[product.id] = 0
        }
        this.bucket[product.id] += amount * product.amount;

        this.$router.navigate(["/product", "edit", product.id])
    }
}
