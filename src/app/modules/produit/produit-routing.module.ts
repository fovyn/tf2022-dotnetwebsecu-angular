import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { DetailComponent } from "./components/detail/detail.component";
import { ListComponent } from "./components/list/list.component";
import { ProduitComponent } from "./produit.component";

const PRODUCT_ROUTES: Routes = [
    {
        path: '', component: ProduitComponent, children: [
            // { path: 'list', component: ListComponent }
            { path: 'edit/:id', component: DetailComponent }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(PRODUCT_ROUTES)],
    exports: [RouterModule]
})
export class ProduitRoutingModule { }