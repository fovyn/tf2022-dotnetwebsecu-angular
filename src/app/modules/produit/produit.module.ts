import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProduitComponent } from './produit.component';
import { ListComponent } from './components/list/list.component';
import { SharedModule } from '../shared/shared.module';
import { ProduitRoutingModule } from './produit-routing.module';
import { DetailComponent } from './components/detail/detail.component';



@NgModule({
    declarations: [
        ProduitComponent,
        ListComponent,
        DetailComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        ProduitRoutingModule
    ],
    exports: [
        ProduitComponent
    ]
})
export class ProduitModule { }
