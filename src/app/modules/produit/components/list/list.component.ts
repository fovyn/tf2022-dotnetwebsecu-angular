import { Component, OnInit, EventEmitter, Output, ViewChild } from '@angular/core';
import { Product } from 'src/app/models/product.model';
import { FormInputType } from 'src/app/modules/shared/components/form-input/form-input.model';
import { MatModalDirective } from 'src/app/modules/shared/directives/mat-modal.directive';
import { LoginModalComponent } from 'src/app/modules/shared/modals/login-modal/login-modal.component';
import { PRODUCTS } from './db.fake';

@Component({
    selector: 'produit-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
    @ViewChild(MatModalDirective) private modal: MatModalDirective | null = null;
    private qtt: number = 0;

    @Output("buy")
    buyEvent = new EventEmitter<{ product: Product, amount: number }>();

    private products: Array<Product> = [...PRODUCTS]
    private buyProduct: Product | undefined = undefined;
    private current: number = 1;
    private pageSize: number = 5;

    get Current(): number { return this.current }
    get Products(): Array<Product> { return this.products; }
    get FormInputType() { return FormInputType }
    get Qtt(): number { return this.qtt }
    get PageSize(): number { return this.pageSize }

    set Qtt(v: number) { this.qtt = v }
    set Current(v: number) { this.current = v; }
    set PageSize(v: number) { this.pageSize = v }

    constructor() { }

    ngOnInit(): void {
    }


    handleBuyAction(id: number) {
        const product = this.Products.find(it => it.id === id);

        this.buyProduct = product;
        this.buyEvent.emit({ product: product as Product, amount: 1 })
    }

    handleChangePage(value: any) {
        if (typeof value == 'string') {
            this.PageSize = parseInt(value);
        } else if (typeof value == 'number') {
            this.PageSize = value
        }
    }
}
