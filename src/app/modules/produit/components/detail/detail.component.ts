import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PRODUCTS } from '../list/db.fake';
import { Product } from './../../../../models/product.model';

@Component({
    selector: 'detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
    private products = PRODUCTS
    private product: Product | undefined = undefined;

    get Product(): Product | undefined { return this.product; }
    constructor(
        private $ar: ActivatedRoute
    ) { }

    ngOnInit(): void {
        this.$ar.paramMap.subscribe(map => {
            const id = map.get("id") as string;
            this.product = this.products.find(it => it.id == parseInt(id))
        })
    }

}
