import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatModalDirective } from './directives/mat-modal.directive';
import { FormsModule } from '@angular/forms';
import { FormInputComponent } from './components/form-input/form-input.component';
import { PagePipe } from './page.pipe';
import { LoginModalComponent } from './modals/login-modal/login-modal.component';
import { PaginatorComponent } from './components/paginator/paginator.component';
import { MatSelectDirective } from './directives/mat-select.directive';



@NgModule({
    declarations: [
        MatModalDirective,
        FormInputComponent,
        PagePipe,
        LoginModalComponent,
        PaginatorComponent,
        MatSelectDirective,
    ],
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: [
        FormsModule,
        MatModalDirective,
        FormInputComponent,
        PagePipe,
        LoginModalComponent,
        PaginatorComponent,
        MatSelectDirective
    ]
})
export class SharedModule { }
