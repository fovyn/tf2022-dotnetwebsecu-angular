import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
    selector: 'paginator',
    templateUrl: './paginator.component.html',
    styleUrls: ['./paginator.component.scss']
})
export class PaginatorComponent implements OnInit {
    private length: number = 0;
    private current: number = 1;
    private pageSize: number = 5;

    @Input("length") set Length(v: number) { this.length = v; }
    @Input("pageSize") set PageSize(v: number) { this.pageSize = v; }
    @Input("current") set Current(v: number) { this.current = v; }

    get Current(): number { return this.current; }
    get PageSize(): number { return this.pageSize; }

    @Output("currentChange")
    private pageChangeEvent = new EventEmitter<number>()

    get Pages(): Array<number> {
        const a: number[] = [];
        let nb = parseInt((this.length / this.PageSize).toString());

        if (this.length % this.PageSize > 0) {
            nb++;
        }

        for (let i = 1; i <= nb; i++) {
            a.push(i)
        }

        return a;
    }

    constructor() { }

    ngOnInit(): void {
    }

    get isLast() {
        let nbPage = parseInt((this.length / this.pageSize).toString());
        if (this.length % this.pageSize > 0) nbPage++;
        return this.current >= nbPage;
    }
    get isFirst() {
        return this.current <= 1;
    }

    next() {
        if (!this.isLast) {
            this.pageChangeEvent.emit(++this.current)
        }
    }
    previous() {
        if (!this.isFirst) {
            this.pageChangeEvent.emit(--this.current)
        }
    }

    selectPage(page: number) {
        this.pageChangeEvent.emit(page);
    }
}
