import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormInputType, FormInputValidator } from './form-input.model';

@Component({
    selector: 'form-input',
    templateUrl: './form-input.component.html',
    styleUrls: ['./form-input.component.scss']
})
export class FormInputComponent implements OnInit {
    @Output("valueChange")
    valueChangeEvent = new EventEmitter<any>();

    private label: string | null = null;
    private type: FormInputType = FormInputType.text;
    private value: any | null = null;
    private validators: Array<FormInputValidator> = [];
    private errors: Array<string> = [];

    get Label(): string | null { return this.label; }
    get Type(): FormInputType { return this.type; }
    get Value(): any | null { return this.value; }
    get Errors(): Array<string> { return this.errors; }

    @Input("label") set Label(v: string | null) { this.label = v; }
    @Input("type") set Type(v: FormInputType) { this.type = v || FormInputType.text; }
    @Input("value") set Value(v: any | null) { this.value = v; }
    @Input("validators") set Validators(v: Array<FormInputValidator>) { this.validators = [...v] }

    constructor() { }

    ngOnInit(): void {
        this.validation(this.Value)
    }
    handleChangeValue(e: any) {
        const { target } = e;

        this.validation(target.value);

        this.Value = target.value;
        this.valueChangeEvent.emit(this.value);
    }

    validField() {
        this.validation(this.Value);
    }

    private validation(value: any) {
        this.errors = [];
        for (let validator of this.validators) {
            const res = validator(value);
            if (res) {
                for (let key in res) {
                    this.errors.push(res[key]);
                }
            }
        }
    }
}
