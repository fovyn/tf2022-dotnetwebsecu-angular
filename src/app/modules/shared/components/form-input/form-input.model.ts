export enum FormInputType {
    text = 'text',
    number = 'number',
    date = 'date',
    password = 'password'
}

export type FormInput = {
    label: string,
    type: FormInputType,
    value: any,
    errors: any
}

export type FormInputValidator = (d: any) => { [key: string]: string } | null