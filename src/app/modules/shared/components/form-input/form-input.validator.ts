function required(msg: string = "Le champs est obligatoire") {
    return (v: any | null) => {
        if (v && v != '') {
            return null;
        } else {
            return { 'required': msg }
        }
    }
}

function minLength(
    length: number,
    msg: string = "La taille doit être gte " + length
) {
    return (v: any | null) => {
        const isEmpty = required()(v);
        if (isEmpty) {
            return { 'minLength': msg, ...isEmpty }
        }
        if (typeof v == 'string') {
            if (v.length >= length) return null;
            return { 'minLength': msg }
        } else {
            return { 'minLength': 'Wrong type' }
        }
    }
}

export const Validators = {
    required,
    minLength
}