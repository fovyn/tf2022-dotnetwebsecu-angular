import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'page'
})
export class PagePipe implements PipeTransform {

    transform(value: Array<any>, current: number, size: number = 5): Array<any> {
        const start = (current - 1) * size;
        return value.slice(start, start + size);
    }

}
