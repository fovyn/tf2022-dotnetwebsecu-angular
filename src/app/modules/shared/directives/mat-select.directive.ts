import { outputAst } from '@angular/compiler';
import { Directive, ElementRef, Output, EventEmitter } from '@angular/core';

@Directive({
    selector: '[matSelect]'
})
export class MatSelectDirective {
    private _instance: M.FormSelect | null = null

    @Output("valueChange")
    private valueChangeEvent = new EventEmitter<any>();

    constructor(
        private $er: ElementRef<HTMLSelectElement>
    ) {
        const _this = this;
        this.$er.nativeElement.addEventListener("change", function (e) {
            _this.valueChangeEvent.emit(this.value)
        })
    }

    ngAfterViewInit() {
        this._instance = M.FormSelect.init(this.$er.nativeElement);
    }

}
