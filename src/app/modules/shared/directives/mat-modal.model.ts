export interface MatModalAction {
    onModalOpen(): void;
    onModalClose(): void;
}