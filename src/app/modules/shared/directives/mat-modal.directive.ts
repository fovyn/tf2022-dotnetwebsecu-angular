import { Directive, ElementRef, Input, ViewContainerRef, ContentChild } from '@angular/core';
import * as M from 'materialize-css'
import { MatModalAction } from './mat-modal.model';

@Directive({
    selector: '[matModal]'
})
export class MatModalDirective {
    @ContentChild('content', { read: ViewContainerRef }) template: ViewContainerRef | null = null;

    private _instance: M.Modal | null = null
    private _options: Partial<M.ModalOptions> = {};
    private _type: any | null = null;
    private _component: any | null = null;

    @Input("options")
    set Options(v: Partial<M.ModalOptions>) { this._options = { ...v } }
    @Input("type")
    set Type(v: any) { this._type = v; }

    get Div(): HTMLDivElement { return this.$er.nativeElement; }
    get Component(): MatModalAction { return this._component.instance as MatModalAction }
    constructor(
        private $er: ElementRef<HTMLDivElement>
    ) {
    }

    ngOnDestroy() {
        this._instance?.destroy()
    }

    open() {
        this._component = this.template?.createComponent<MatModalAction>(this._type)


        const onOpenStart = this.Component.onModalOpen
        const onCloseStart = this.Component.onModalClose
        const options = { inDuration: 2000, outDuration: 3000, onOpenStart, onCloseStart, ...this._options };

        if (this._instance) {
            this._instance.destroy();
            this._instance = null;
        }

        this._instance = M.Modal.init(
            this.Div,
            options
        )

        this._instance?.open();
    }

    close() {
        this._instance?.close();
    }

}
