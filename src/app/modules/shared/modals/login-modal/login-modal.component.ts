import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormInputType } from 'src/app/modules/shared/components/form-input/form-input.model';
import { MatModalAction } from './../../directives/mat-modal.model';

@Component({
    selector: 'login-modal',
    templateUrl: './login-modal.component.html',
    styleUrls: ['./login-modal.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginModalComponent implements OnInit, MatModalAction {
    Form = { username: null, password: null }
    get FormInputType(): any { return FormInputType }

    constructor() { }
    onModalOpen(): void {
        console.log("OPEN")
    }
    onModalClose(): void {
        console.log("CLOSE")
    }

    ngOnInit(): void {
    }

}
